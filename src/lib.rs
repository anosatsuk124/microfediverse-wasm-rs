use crate::bindgen::*;
use crate::json::*;
use wasm_bindgen::prelude::*;
mod bindgen;
mod json;

#[wasm_bindgen(start)]
pub fn main() {
    log(format!("Helloworld"));
    let activitypub = ActivityPub {
        context: "https://www.w3.org/ns/activitystreams".to_string(),
        r#type: "Person".to_string(),
        id: "https://example.com/test".to_string(),
        name: "test".to_string(),
        preferredUsername: "test".to_string(),
        summary: "my simple activitypub".to_string(),
        inbox: "https://example.com/inbox".to_string(),
        outbox: "https://example.com/outbox".to_string(),
        url: "https://example.com/test".to_string(),
        icon: Icon {
            r#type: "Image".to_string(),
            mediaType: "image/png".to_string(),
            url: "https://example.com/icon.ong".to_string(),
        },
    };

    log(format!("{}", serde_json::to_string(&activitypub).unwrap()));
}
