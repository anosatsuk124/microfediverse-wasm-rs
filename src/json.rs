use serde::Serialize;

type Url = String;
type Type = String;

#[derive(Debug, Serialize)]
pub struct ActivityPub {
    #[serde(rename(serialize = "@context"))]
    pub context: Url,
    pub r#type: Type,
    pub id: Url,
    pub name: String,
    pub preferredUsername: String,
    pub summary: String,
    pub inbox: Url,
    pub outbox: Url,
    pub url: Url,
    pub icon: Icon,
}

#[derive(Debug, Serialize)]
pub struct Icon {
    pub r#type: String,
    pub mediaType: String,
    pub url: Url,
}
